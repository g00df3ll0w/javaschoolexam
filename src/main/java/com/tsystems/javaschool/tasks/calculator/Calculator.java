package com.tsystems.javaschool.tasks.calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(final String statement) {
        if (statement == null || !test(statement)) return null;
        Double result = new Object() {
            int pos = -1, character;

            void nextChar() {
                character = (++pos < statement.length()) ? statement.charAt(pos) : -1;
            }

            boolean pass(int charToPass) {
                if (character == charToPass) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < statement.length()) x = Double.POSITIVE_INFINITY; // Unexpected character
                return x;
            }

            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if      (pass('+')) x += parseTerm(); // addition
                    else if (pass('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if      (pass('*')) x *= parseFactor(); // multiplication
                    else if (pass('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                double x;
                int startPos = this.pos;
                if (pass('(')) { // parentheses
                    x = parseExpression();
                    if (!pass(')')) x = Double.POSITIVE_INFINITY; //closed bracket doesn't exist
                } else if ((character >= '0' && character <= '9') || character == '.') { // numbers
                    while ((character >= '0' && character <= '9') || character == '.') nextChar();
                    try {
                        x = Double.parseDouble(statement.substring(startPos, this.pos));
                    }
                    catch (NumberFormatException e) { //catch multiply point
                        x = Double.POSITIVE_INFINITY;
                    }
                } else {
                    return Double.POSITIVE_INFINITY;
                }
                return x;
            }
        }.parse();
        if (result % 1 == 0) return Integer.toString(result.intValue());
        else if (Double.isInfinite(result)) return null;
        else return result.toString();
    }

    public static boolean test(String testString){
        String regex = "[()/*+\\-0-9.]*";
        String regex1 = "^.*?//+.*$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(testString);

        Pattern p1 = Pattern.compile(regex1);
        Matcher m1 = p1.matcher(testString);

        return (!m1.matches() && m.matches());
    }
}
