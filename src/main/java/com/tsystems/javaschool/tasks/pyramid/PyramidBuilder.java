package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException | OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
        int sizeArr = inputNumbers.size();
        int[] array = new int[sizeArr];
        for (int i = 0; i < sizeArr; i++)
            if (inputNumbers.get(i) != null)
                array[i] = inputNumbers.get(i);
            else throw new CannotBuildPyramidException();

        return pyramid(countH(sizeArr), array);
    }

    private int countH(int sizeArr) {
        int cnt = sizeArr;
        for (int i = 1;i<sizeArr;i++) {
            if (cnt == i) {
                return i;
            } else if (cnt < i) {
                throw new CannotBuildPyramidException();
            } else {
                cnt -= i;
            }
        }
        return 0;
    }

    private int[][] pyramid(int h, int[] startArr) {
        int[][] arr = new int[h][h * 2 - 1];
        int positionArr = 0;
        int leftOffset;
        for (int i = 0; i < h; i++) {
            leftOffset = h - i - 1;
            arr[i][leftOffset] = startArr[positionArr++];
            for (int g = 0; g < i; g++) {
                leftOffset += 2;
                arr[i][leftOffset] = startArr[positionArr++];
            }
        }
        return arr;
    }
}
