package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int numberFirstSequence;
        int sizeY;
        try {
            if (!x.isEmpty() && y.isEmpty()) {
                return false;
            }
            numberFirstSequence = x.size();
            sizeY = y.size();
        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
        int current = 0;
        while (numberFirstSequence > 0) {
            for (int i = current; i < sizeY; i++) {
                if (numberFirstSequence > 0) {
                    if (y.get(i).equals(x.get(x.size() - numberFirstSequence))) {
                        current = i;
                        numberFirstSequence--;
                    } else if (i == sizeY - 1)
                        return false;
                }
            }
        }
        return true;
    }
}
